# Executable Code Snippets

## Requirements

* Python 3.8 (feel free to adapt to older and newer versions); all Python dependencies should be installed automatically. If you have multiple Python versions installed, make sure to use the right one.

* For C++ snippets: GCC

## Installation

Install either directly from the Git repo, e.g.

    pip install --upgrade git+https://gitlab.ethz.ch/dinfk-lecturers/executable-code-snippets.git

or download and unzip the sources, e.g. into `~/executable-code-snippets` and then install from there:

    pip install --upgrade -e ~/executable-code-snippets

Afterwards, you should be able to run `xcs-convert`:

    $ xcs-convert
    usage: xcs-convert [-h] file
    xcs-convert: error: the following arguments are required: file

## Usage

1. Copy `examples/various.md`, from this repository, to, e.g. `~/xcs/various.md`
2. `cd ~/xcs` and then `xcs-convert various.md`
3. Open `out/various.html` in your browser

## License & Contributions

Sources are released under Mozilla Public License 2.0.

Contributions are welcome.
