# Question 1

Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. Lorem *ipsum dolor* sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. 

```c++
double h = 10.0;
float m = 2.0f;
std::cout << h / m * 3;
```

Which answer describes the output best?

1. `0.0f`
2. `6`
3. `15.0`
4. `ERROR`



# Question 2

```c++
std::vector<double> v = {2.0, 5.0, -2.0, 2.0};
std::cout << v[2] / v[3];
```

Which answer describes the output best?

1. Undefined
2. `-2.0/2.0`
3. `0.0`
4. `-1`



# Question 3

Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea *commodo* consequat. Duis **autem** vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.

```c++
int r = 10;
int h = 1;
std::cout << (
  (r % 3) == h && ((r % 2) == 0 || r != h));
```

Which answer describes the output best?

1. `true`
2. `false`
3. `ERROR`



# Question 4 (Snippet doesn't compile)

Consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum.

```c++
std::set names = {"Floyd", "Mona"};
std::cout << *names.cbegin();
```



# Question 5 (Snippet not compiled)

At vero eos et accusam et justo duo dolores et ea rebum.

```cpp
int foo;
...
bool foo;
```

Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. 

Which answer is correct?

1. Does not compile
2. Compiles, `foo` will be an `int`, since first declaration)
3. Compiles; `foo` will be a `bool`, since last declaration
4. Compiles; `foo` will be a `int`, since `int` is the larger/more general type



# Question 6 (Snippet with global declaration)

```c++
int& foo(std::vector<int>& vec, unsigned int i) {
  assert(0 <= i && i < vec.size());
  return vec.at(i);
}
/* --- main --- */
std::vector<int> xs = {1,0,-1,-2};
foo(xs, foo(xs, 0)) = 0;
std::cout << xs.at(0);
```

Which output is generated?

1. `1`
1. `0`
1. `-1`
1. `-2`
1. `2`



# Question 7 (Marker no_show)

```c++
#include <array> /* --- no_show --- */
using intseq = std::array<int, 3>; /* --- no_show --- */
/* --- main --- */
intseq numbers = {1,2,3};
std::cout << numbers.size();
```



# Question 8 (Marker no_compile)

```c++
/* --- main --- */
unsigned int u = ...; // any value /* --- no_compile --- */
unsigned int u = -1; /* --- no_show --- */
std::assert(u < u + 1);
```
