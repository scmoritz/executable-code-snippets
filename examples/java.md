# Simple Java example

```java
int x = 3;
System.out.println(8 / x);
```

Which answer describes the result best?

1. `2`
1. `2.6`
1. `ERROR`



# Java with import — NOT YET SUPPORTED

```java
import java.util.regex.*;
/* --- main --- */
String[] result = 
  Pattern.compile("[,\\s]+")
         .split("one,two, three   four ,  five");
System.out.println(result.length);
```

Which answer describes the result best?

1. `ERROR`
1. `1`
1. `2`
1. `3`
1. `4`
