import os
import sys

script_name = os.path.split(sys.argv[0])[1]
if sys.version_info <= (3, 8):
  print('ERROR: Running {} with Python below 3.8 is not supported'.format(script_name))
  sys.exit(1)

import logging
import jinja2
import markdown
import time
import shutil
import warnings
import traceback
import argparse
import pkgutil
import re
import base64
import pymdownx.superfences

from .compilation import gcc, java
from .utils.files import *
from .utils.database import ResultDB
from .utils import logging as logutils


# import logging
# logging.basicConfig()
# logging.getLogger('MARKDOWN').setLevel(logging.DEBUG)

OUTPUT_DIR='out'
TEMP_DIR='tmp'
DB_FILE='xcs.db'


def source_validator(language, inputs, options, attrs, md):
  for key, value in inputs.items():
    options[key] = value
  return True

def source_formatter(source, language, css_class, options, md, **kwargs):
  try:
    result = actual_source_formatter(source, language, css_class, options, md, **kwargs)
    sys.stdout.flush()
    return result
  except Exception:
    print(traceback.format_exc(), file=sys.stderr)
    sys.exit(1)

COMPILABLE_SNIPPETS = {
  'c++': gcc.GccCompilableSnippet,
  'java': java.JavaCompilableSnippet,
}

EXTENSION_CONFIGS = {
  'markdown.extensions.codehilite': {},
  'markdown.extensions.smarty': {},
  'pymdownx.superfences': {
    'custom_fences': [
      {
        'name': 'c++',
        'class': 'highlight',
        'format': source_formatter,    # Functions passed here must
        'validator': source_validator, # already be declared
      },
      {
        'name': 'java',
        'class': 'highlight',
        'format': source_formatter,    # Functions passed here must
        'validator': source_validator, # already be declared
      },
    ]
  },
}

EXTENSIONS = list(EXTENSION_CONFIGS.keys())

current_markdown_file = None
resultdb = None

def actual_source_formatter(source, language, css_class, options, md, **kwargs):
  logging.debug(f'Snippet source formatter called ({language})')
  # logging.debug("  language = {}".format(language))
  # logging.debug("  options = {}".format(options))
  # logging.debug("  kwargs = {}".format(kwargs))
  # logging.debug("  source = {}".format(source))

  formatter = None

  for ext in md.registeredExtensions:
    if (isinstance(ext, pymdownx.superfences.SuperFencesCodeExtension)):
      formatter = ext.superfences[0]['formatter']

  # print("  formatter = {}".format(formatter))
  assert \
    formatter != None, \
    'Failed to find an instance of pymdownx.superfences.SuperFencesCodeExtension'

  snippet_name = None
  if 'name' in options:
    snippet_name = options['name'] 
    del options['name']

  snippet = COMPILABLE_SNIPPETS[language](
    source,
    options,
    resultdb,
    snippet_name,
    OUTPUT_DIR,
    TEMP_DIR)

  logging.info(f'Found {"unchanged" if snippet.cached else "new/changed"} {language} snippet {snippet.name}')

  snippet.preprocess()
  processed_code = snippet.get_processed_code()

  source_html = formatter(processed_code, language, options, md, **kwargs)
  # print("  source_html = {}".format(source_html))

  render_success = snippet.render_image()
  assert render_success, f'Did not expect snippet image rendering to fail'

  # TODO: Current assumption is that the image was generated in the same directory
  #       where the final HTML file is located. Instead, the path to the image should
  #       be made relative to the HTML file. Python probably provides a function for that.
  image_filename = snippet.get_image_filename()
  # image_html = f'<img src="{get_filename(image_filename)}" title="{get_filename(image_filename)}">'
  image_data = base64.b64encode(open(image_filename, 'rb').read()).decode()

  image_styles = [
    'max-width: 100%;', # Unprefixed (but not the desired effect)
    'max-width: -moz-available;', # For Mozilla
    'max-width: -webkit-fill-available;', # /* For Chrome */
    'max-width: stretch;', # Unprefixed (but not generally supported)
  ]

  image_style = '; '.join(image_styles)

  image_html = f'<img src="data:image/png;base64,{image_data}" title="{get_filename(image_filename)}" style="{image_style}">'

  compiler_output_html = '<div class="no-result">compiler not executed</div>'
  executable_output_html = '<div class="no-result">snippet not executed</div>'

  if 'no_compile' not in options:
    compilation_success = snippet.compile()
    compiler_output = snippet.get_compiler_output()

    if 'no_run' not in options and compilation_success: # Snippet compiled successfully
      compiler_output_html = ''

      snippet.execute()
      executable_output = snippet.get_execution_output()

      executable_output_formatted = formatter(
        executable_output, 'plain', [], md, **kwargs) # TODO: Pass empty kwargs

      executable_output_html = f'''
          <div class="executable-output">
            {executable_output_formatted}
          </div>
        '''.strip()
    else: # Snippet did not compile
      compiler_output_formatted = formatter(
        compiler_output, 'plain', [], md, **kwargs) # TODO: Pass empty kwargs

      compiler_output_html = f'''
        <div class="compiler-output">
          {compiler_output_formatted}
        </div>'''.strip()

  return f'''
    <div class="snippet-block">
      {source_html}
      {compiler_output_html}
      {executable_output_html}
      {image_html}
    </div>
    '''.strip()





## Parameter html is expected to be a string, not an HTML DOM tree.
def replace_language_tags(html):
  def repl(match):
    assert match.group(1) # DE must be used
    assert 1 <= match.lastindex <= 2 # EN is optional

    de = '<span lang="de">{}</span>'
    en = '<span lang="en" style="color: #6633ff;">{}</span>'

    if match.group(1): de_res = de.format(match[1])
    if match.group(2): en_res = en.format(match[2])

    if match.lastindex == 1: res = de_res
    else: res = f'{de_res} / {en_res}'

    return f'<span class="copy-to-clipboard" onclick="updateClipboard(this)">{res}</span>'

  html = re.sub(
    r'''(?ms)<de>(.*?)</de>(\s*<en>(.*?)</en>)?''',
    # r'''<span class="copy-to-clipboard" onclick="updateClipboard(this)"> / <span lang="en" style="color: #A300A3;">\2</span></span>''',
    # r'''<span class="copy-to-clipboard" onclick="updateClipboard(this)"><span lang="de">\1</span> / <span lang="en" style="color: #A300A3;">\2</span></span>''',
    repl,
    html)

  return html


def set_up_directories(clean_first):
  if clean_first:
    if os.path.exists(OUTPUT_DIR) and os.path.isdir(OUTPUT_DIR): shutil.rmtree(OUTPUT_DIR)
    if os.path.exists(TEMP_DIR) and os.path.isdir(TEMP_DIR): shutil.rmtree(TEMP_DIR)

  os.makedirs(OUTPUT_DIR, exist_ok=True)
  os.makedirs(TEMP_DIR, exist_ok=True)

def configure_cli_parser(parser):
  parser.add_argument(
    '--clean',
    default=False, 
    action='store_true',
    help='Clean temporary and output directory before conversion')
  
  parser.add_argument(
    'file',
    type=argparse.FileType('r', encoding='UTF-8'),
    help='Markdown file with code snippets')

  logutils.add_loglevel_argument(parser)

def main():
  parser = argparse.ArgumentParser()
  configure_cli_parser(parser)
  args = parser.parse_args()

  logutils.configure_level_and_format(args.log_level)

  set_up_directories(args.clean)

  resultdb_filepath = f'{TEMP_DIR}/{DB_FILE}'
  jinja_template = None
  source_markdown = args.file.read()

  global current_markdown_file, resultdb
  current_markdown_file = args.file.name
  resultdb = ResultDB(resultdb_filepath)

  ## Show all entries in resultdb
  # for k in resultdb.db.keys():
  #   logging.debug(f'{k} ~~~> {resultdb.db[k]}')

  source_html = markdown.markdown(
    source_markdown,
    output_format='html5',
    extensions=EXTENSIONS,
    extension_configs=EXTENSION_CONFIGS)

  source_html = replace_language_tags(source_html)

  output_filename = with_extension(get_filename(args.file.name), 'html')
  output_filepath = f'{OUTPUT_DIR}/{output_filename}'

  if not jinja_template:
    template_file = os.path.join('./data/theme/templates', 'questions.html')
    jinja_template = jinja2.Template(pkgutil.get_data(__name__, template_file).decode('utf-8'))

  jinja_output = jinja_template.render(
    title='Questions',
    base_url='.',
    content=source_html,
    last_updated=time.strftime("%Y-%m-%d %H:%M GMT", time.gmtime()))

  with open(output_filepath, 'w', encoding='utf-8', errors='xmlcharrefreplace') as fh:
    fh.write(jinja_output)

  resultdb.close()

  # TODO: Avoid redundant copies
  # shutil.copytree('theme/static', f'{OUTPUT_DIR}/theme', dirs_exist_ok=True)
  static_path = os.path.dirname(__file__) + '/data/theme/static'
  shutil.copytree(static_path, f'{OUTPUT_DIR}/theme', dirs_exist_ok=True)



if __name__ == "__main__":
  main()
