#include <iostream>
#include <cassert>
#include <vector>
#include <set>
#include <unordered_set>

namespace xcs {
  std::string dec_to_base(int n, std::size_t base = 10) {
    const std::string digits = "0123456789abcdef";
    std::string result = "";

    if (base < 2 || base > 16)
      throw std::invalid_argument("invalid base");
    
    int sign;
    if ((sign = n) < 0) // Record sign
      n = -n; // Make n positive
    
    do {
      result = digits[n % base] + result; // Get next digit
    } while ((n /= base) > 0); // Reduce number to convert
    
    if (sign < 0)
      result = "-" + result;
      
    return result;
  }
}

/*{{ GLOBAL }}*/

int main() {
  /*{{ MAIN }}*/

  return 0;
}
