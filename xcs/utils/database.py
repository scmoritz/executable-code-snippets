import shelve

class ResultDB():
  def __init__(self, db_path):
    self.db_path = db_path
    self.db = shelve.open(db_path, 'c')

  def __contains__(self, key):
    return key in self.db

  def __getitem__(self, key):
    return self.db[key]

  def __setitem__(self, key, value):
    self.db[key] = value

  def __delitem__(self, key):
    del self.db[key]

  def get_view_for(self, uid):
    return ResultDBView(self, uid)

  def close(self):
    self.db.close()


class ResultDBView():
  def __init__(self, source_db, uid):
    self.source_db = source_db
    self.uid = uid

  def _ukey(self, key):
    return f'{self.uid}.{key}'

  def __contains__(self, key):
    return self._ukey(key) in self.source_db

  # Argument 'arg' can be a key, or a tuple of (key, default value).
  # In the second case, the default value is returned if the key is not present
  # in this database view.
  def __getitem__(self, arg):
    if isinstance(arg, tuple):
      assert len(arg) == 2, f'Expected pair of (key, default value), but got {key}'
      key, default = arg
      ukey = self._ukey(key)
      if ukey in self.source_db:
        return self.source_db[ukey]
      else:
        return default
    else:
      # print(f'arg = {arg}')
      # print(f'self._ukey(arg) = {self._ukey(arg)}')
      return self.source_db[self._ukey(arg)]

  def __setitem__(self, key, value):
    self.source_db[self._ukey(key)] = value

  def __delitem__(self, key):
    del self.source_db[self._ukey(key)]
