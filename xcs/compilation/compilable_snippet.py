from abc import ABC, abstractmethod
import logging
import os
import re
import hashlib
import base64
import datetime
import dataclasses

from ..utils.files import *
from ..utils.database import ResultDBView


class CompilableSnippet(ABC):
  # TODO: Include mandatory properties, e.g. processed_code

  @abstractmethod
  def preprocess(self):
    pass

  @abstractmethod
  def get_processed_code(self):
    pass

  @abstractmethod
  def compile(self, compilation_template_file=None):
    pass

  @abstractmethod
  def compile_succeeded(self):
    pass

  @abstractmethod
  def get_compiler_output(self):
    pass

  @abstractmethod
  def execute(self):
    pass

  @abstractmethod
  def execute_succeeded(self):
    pass

  @abstractmethod
  def get_execution_output(self):
    pass

  # TODO: Factor out into separate class, since unrelated from compiling
  @abstractmethod
  def render_image(self, png_filename=None, snippet_filename=None, pygmentize_options={}):
    pass

  @abstractmethod
  def render_image_succeeded(self):
    pass

  @abstractmethod
  def get_image_filename(self):
    pass

@dataclasses.dataclass
class AbstractCompilableSnippetCache(object):
  last_used: datetime.datetime
  options: object ## TODO: dict[str, str]
  processed_code: str
  compilation_succeeded: bool
  compilation_log_filename: str
  execution_succeeded: bool
  execution_log_filename: str
  rendering_succeeded: bool
  rendering_log_filename: str  ## TODO: Avoid. Not actually necessary, but set by _exec
  rendering_png_filename: str

  def __init__(self, db):
    assert isinstance(db, ResultDBView)
    self.db = db

  def __getattr__(self, key):
    if key in self.__dataclass_fields__:
      return self.db[key, None]
    else:
      raise AttributeError(key)

  def __setattr__(self, key, value):
    if key in self.__dataclass_fields__:
      self.db[key] = value
    else:
      return object.__setattr__(self, key, value)

  @staticmethod
  def _ifthen(lhs, *rhs):
    return not lhs or all(rhs)

  def is_consistent(self):
    ## Could be written more readably if Python had a short-circuiting implication
    ## operator, or would allow implementing one that resulted in a nice call syntax.

    con = (
      self.last_used and
      self.options != None and
      self.processed_code and
      self.rendering_succeeded != None and
      self.rendering_log_filename and
      file_exists(self.rendering_log_filename))

    if con and self.rendering_succeeded:
      con = self.rendering_png_filename and file_exists(self.rendering_png_filename)

    if con and 'no_compile' not in self.options:
      con = (
        self.compilation_succeeded != None and
        self.compilation_log_filename and
        file_exists(self.compilation_log_filename))
      
      if con and self.compilation_succeeded:
        con = (
          self.execution_succeeded != None and
          self.execution_log_filename and
          file_exists(self.execution_log_filename))

    return con

  def touch(self):
    self.last_used = datetime.datetime.now()

  def clear(self):
    for key in self.__dataclass_fields__: 
      if key in self.db: 
        del self.db[key]

class AbstractCompilableSnippet(CompilableSnippet):
  @property
  @abstractmethod
  def LANGUAGE_FILE_EXTENSION(self): pass # For file extensions

  @property
  @abstractmethod
  def LANGUAGE_PYGMENTS(self): pass # Passed to Pygments

  def __init__(self, code, options, resultdb, snippet_name=None, out_dir='out', tmp_dir='tmp'):
    # Placeholders in compilation template
    self.GLOBAL_PLACEHOLDER = '/*{{ GLOBAL }}*/'
    self.MAIN_PLACEHOLDER = '/*{{ MAIN }}*/'

    # Multiline-comment on one line, with "main" in the centre, surrounded by at least
    # three dashes (and whitespace) on each side.
    # E.g. "/* --- main --- */" (without quotes).
    self.GLOBAL_MAIN_SEPARATOR_REGEX = r'/\*\s*-{3,}\s*main\s*-{3,}\s*\*/'
    # Analogous, e.g. "/* --- no_compile --- */"
    self.NO_SHOW_MARKER_REGEX = r'/\*\s*-{3,}\s*no_show\s*-{3,}\s*\*/'
    self.NO_COMPILE_MARKER_REGEX = r'/\*\s*-{3,}\s*no_compile\s*-{3,}\s*\*/'

    self.code = code

    ## TODO: Make fields for internal use private

    hash = hashlib.sha256()
    hash.update(self.code.encode('utf-8'))
    hash.update(str(options).encode('utf-8'))
    checksum = hash.digest()
    checksum = base64.urlsafe_b64encode(checksum).decode('utf-8')
    checksum = checksum.replace('=', '')

    if snippet_name:
      self.name = snippet_name
      logging.debug(f'Initialising snippet {snippet_name} with checksum {checksum}')
    else:
      self.name = checksum
      logging.debug(f'Initialising snippet with checksum {checksum}')

    ## files_basename is expected to uniquely identify a snippet
    self.files_basename = checksum

    self.cache = AbstractCompilableSnippetCache(resultdb.get_view_for(checksum))
    
    cache_consistent = self.cache.is_consistent()
    if not cache_consistent:
      self.cache.clear()

    self.cache.touch()
    self.cache.options = options

    self.cached = cache_consistent
    logging.debug(f'Snippet {self.name} was{" " if self.cached else " not "}cached')

    self.out_dir = out_dir
    self.tmp_dir = tmp_dir

    os.makedirs(self.out_dir, exist_ok=True)
    os.makedirs(self.tmp_dir, exist_ok=True)

    self.global_code_part = None
    self.main_code_part = None

    # Set by _create_compilable_file()
    self.compilation_filename = None
    # Set by compile()
    self.compilation_command = None
    self.executable_filename = None
    # Set by execute()
    self.execution_command = None
    # Set by render_image()
    self.rendering_snippet_filename = None
    self.rendering_command = None

  ## Parameter text can either be a string (with newlines), or a list of
  ## lines (i.e. strings without newlines). The return type matches the
  ## input type: either a string, or a list of lines.
  @staticmethod
  def _remove_lines_matching(text, regex):
    assert \
      isinstance(text, str) or isinstance(text, list), \
      f'Parameter text must be of type str or list, but got {type(text)}'

    if isinstance(text, str):
      lines = text.splitlines()
    else:
      lines = text

    filtered_lines = [line for line in lines if not re.search(regex, line)]

    if isinstance(text, str):
      return '\n'.join(filtered_lines)
    else:
      return filtered_lines

  def preprocess(self):
    if self.cached:
      logging.debug(f'Skipping preprocessing cached snippet {self.name}')
      return

    logging.debug(f'Preprocessing snippet {self.name}')
    logging.debug('Source snippet length: {} lines'.format(self.code.count("\n")))

    code_parts = re.split(self.GLOBAL_MAIN_SEPARATOR_REGEX, self.code)

    assert 1 <= len(code_parts) <= 2, f'Unexpectedly many global-main separators'
    logging.debug(f'Split code into {len(code_parts)} part(s)')

    if len(code_parts) == 1:
      global_code_part = ''
      main_code_part = self.code
    else:
      global_code_part = code_parts[0]
      main_code_part = code_parts[1]

    global_code_part = self._remove_lines_matching(global_code_part, self.NO_COMPILE_MARKER_REGEX)
    main_code_part = self._remove_lines_matching(main_code_part, self.NO_COMPILE_MARKER_REGEX)

    compilation_snippet_length = global_code_part.count("\n") + main_code_part.count("\n")
    logging.debug(f'Compilation snippet length: {compilation_snippet_length} lines')

    code_without_separator = re.sub(self.GLOBAL_MAIN_SEPARATOR_REGEX, '', self.code)
    processed_code = self._remove_lines_matching(code_without_separator, self.NO_SHOW_MARKER_REGEX)
    processed_code = re.sub(self.NO_COMPILE_MARKER_REGEX, '', processed_code)

    logging.debug('Processed snippet length: {} lines'.format(processed_code.count("\n")))

    self.global_code_part = global_code_part
    self.main_code_part = main_code_part
    self.cache.processed_code = processed_code

  def get_processed_code(self):
    return self.cache.processed_code

  ## Parameter prefix must be a string such that <prefix>_succeeded and
  ## <prefix>_log_filename etc. are valid fields of self.cache.
  def _exec(self, command, log_filename, prefix):
    result = None

    logging.debug(f'Executing "{" ".join(command)}"')
    logging.debug(f'Output will be logged to {log_filename}')

    with open(log_filename, 'w') as fh:
      result = subprocess.run(command, stdout=fh, stderr=subprocess.STDOUT)
      logging.debug(f'Execution terminated with returncode {result.returncode}')

    setattr(self, f'{prefix}_command', command)
    setattr(self, f'{prefix}_result', result)
    setattr(self.cache, f'{prefix}_succeeded', result.returncode == 0)
    setattr(self.cache, f'{prefix}_log_filename', log_filename)
    setattr(self.cache, f'{prefix}_log_timestamp', file_timestamp(log_filename))

    return result

  def _create_compilable_file(self, compilation_template_file=None):
    logging.debug(f'Creating compilable source file for snippet {self.name}')

    if not compilation_template_file:
      compilation_template = self._get_default_compilation_template()
    else:
      compilation_template = pathlib.Path('compilation_template_file').read_text()

    assert compilation_template, 'Failed to read compilation template file'

    compilation_code = \
      compilation_template.replace(self.GLOBAL_PLACEHOLDER, self.global_code_part) \
                          .replace(self.MAIN_PLACEHOLDER, self.main_code_part)

    compilation_filename = f'{self.tmp_dir}/{self.files_basename}.{self.LANGUAGE_FILE_EXTENSION}'

    logging.debug(f'Writing compilable source file {compilation_filename}')
    write_file(pathlib.Path(compilation_filename), compilation_code)

    self.compilation_filename = compilation_filename

  def _compile_final(self, result):
    """Empty default implemention"""
    return result.returncode == 0

  def compile(self, compilation_template_file=None):
    if self.cached:
      logging.debug(f'Skipping compiling cached snippet {self.name}')
      return self.cache.compilation_succeeded

    logging.debug(f'Compiling snippet {self.name}')

    self._create_compilable_file(compilation_template_file)
    assert self.compilation_filename # Set by self._create_compilable_file()

    compilation_filename = self.compilation_filename
    executable_filename = with_extension(compilation_filename, 'exe')
    log_filename = with_extension(compilation_filename, 'log')
    command = self._get_compiler_command(compilation_filename, executable_filename)

    result = self._exec(command, log_filename, 'compilation')

    if result.returncode == 0:
      logging.debug(f'Compilation terminated successfully')
      self.executable_filename = executable_filename
    else:
      logging.warn(f'Compilation of snippet {self.name} failed with returncode {result.returncode}')
      self.executable_filename = None

    final_success = self._compile_final(result)
    self.cache.compilation_succeeded = final_success

    return self.compile_succeeded()

  def compile_succeeded(self):
    return self.cache.compilation_succeeded

  def get_compiler_output(self):
    return read_file(self.cache.compilation_log_filename)

  def execute(self):
    if self.cached:
      logging.debug(f'Skipping executing cached snippet {self.name}')
      return self.cache.execution_succeeded

    logging.debug(f'Executing snippet {self.name}')

    assert \
      self.cache.compilation_succeeded, \
      f'Compilation of snippet {self.name} failed (or was not attempted)'

    assert \
      self.executable_filename, \
      f'Executable for snippet {self.name} was not set'

    executable_filename = self.executable_filename
    log_filename = with_extension(executable_filename, 'log')
    command = self._get_execution_command(executable_filename)

    result = self._exec(command, log_filename, 'execution')

    if result.returncode == 0:
      logging.debug(f'Execution terminated successfully')
    else:
      logging.warn(f'Execution of snippet {self.name} ({executable_filename}) failed with returncode {result.returncode}')

    return self.execute_succeeded()

  def execute_succeeded(self):
    return self.cache.execution_succeeded

  def get_execution_output(self):
    return read_file(self.cache.execution_log_filename)

  def render_image(self, png_filename=None, snippet_filename=None, pygmentize_options={}):
    if self.cached:
      logging.debug(f'Skipping rendering image for cached snippet {self.name}')
      return self.cache.rendering_succeeded

    logging.debug(f'Rendering image for snippet {self.name}')

    if not png_filename:
      png_filename = f'{self.out_dir}/{self.files_basename}.png'

    if not snippet_filename:
      snippet_filename = f'{self.tmp_dir}/{self.files_basename}_png.{self.LANGUAGE_FILE_EXTENSION}'

    self.rendering_snippet_filename = snippet_filename

    logging.debug(f'Writing processed snippet {self.name} to {snippet_filename}')

    write_file(snippet_filename, self.cache.processed_code)

    effective_pygmentize_options = {
      'font_size': 26,
      'line_numbers': True,
      # TODO: Could use matplotlib.font_manager to find out if a nicer source code font
      #       is installed, and if so, specify it here.
    }

    effective_pygmentize_options.update(pygmentize_options)

    pygmentize_options_str = \
      ','.join([f'{k}={v}' for (k, v) in effective_pygmentize_options.items()])

    command = [
      'pygmentize',
      '-f', 'png',
      '-O', pygmentize_options_str,
      '-l', self.LANGUAGE_PYGMENTS,
      '-o', png_filename,
      snippet_filename]

    log_filename = with_extension(snippet_filename, 'log')

    result = self._exec(command, log_filename, 'rendering')

    if result.returncode == 0:
      logging.debug(f'Image rendering terminated successfully')
      png_timestamp = file_timestamp(png_filename)
      self.cache.rendering_png_filename = png_filename
      self.cache.rendering_png_timestamp = png_timestamp
    else:
      logging.warn(f'Rendering image for snippet {self.name} ({png_filename}) failed with returncode {result.returncode}')

    return self.render_image_succeeded()

  def render_image_succeeded(self):
    return self.cache.rendering_succeeded

  def get_image_filename(self):
    return self.cache.rendering_png_filename
