import os
import pkgutil

from . import AbstractCompilableSnippet


class GccCompilableSnippet(AbstractCompilableSnippet):
  LANGUAGE_FILE_EXTENSION = 'cpp'
  LANGUAGE_PYGMENTS = 'cpp'

  def _get_default_compilation_template(self):
    template_file = os.path.join('../data/compilation', 'main.cpp')
    template_content = pkgutil.get_data(__name__, template_file).decode('utf-8')

    return template_content

  def _get_compiler_command(self, compilation_filename, executable_filename):
    return ['g++', compilation_filename, '-o', executable_filename]

  def _get_execution_command(self, executable_filename):
    return [executable_filename]
