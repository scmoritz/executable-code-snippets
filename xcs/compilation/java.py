import os
import pkgutil

from . import AbstractCompilableSnippet
from ..utils.files import *


class JavaCompilableSnippet(AbstractCompilableSnippet):
  LANGUAGE_FILE_EXTENSION = 'java'
  LANGUAGE_PYGMENTS = 'java'
  JAVA_MAIN_CLASS = 'Main' # TODO: May not be matcha user-provided compilation template

  def _get_default_compilation_template(self):
    template_file = os.path.join('../data/compilation', 'main.java')
    template_content = pkgutil.get_data(__name__, template_file).decode('utf-8')

    return template_content

  def _get_compiler_command(self, compilation_filename, executable_filename):
    # Note: Resulting class file will be named after the compilation template,
    # not after the current snippet. Hence, if multiple snippets are compiled,
    # later snippets will overwrite the class files of earlier ones. This is not
    # a problem, due to the subsequent jar'ing of the class file (see
    # self._compile_final()).
    return ['javac', compilation_filename]

  def _get_execution_command(self, executable_filename):
    assert self.jar_filename != None

    return ['java', '-cp', self.jar_filename, self.JAVA_MAIN_CLASS]

  def _compile_final(self, result):
    if result.returncode != 0:
      return 1 # Error

    class_filename = f'{self.JAVA_MAIN_CLASS}.class'
    jar_filename = f'{self.tmp_dir}/{self.files_basename}.jar'
    log_filename = f'{jar_filename}.log'

    self.executable_filename = f'{self.tmp_dir}/{class_filename}'

    command = ['jar', '-cf', jar_filename, '-C', self.tmp_dir, class_filename]
    result = execute(command, log_filename, self, 'bundling')

    if result.returncode == 0:
      logging.debug(f'Jar terminated successfully')
      self.jar_filename = jar_filename
    else:
      logging.warn(f'Jar for snippet {self.fqid} failed with returncode {result.returncode}')
      self.jar_filename = None

    return result.returncode
